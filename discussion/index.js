 // alert("Hello World");/
 // commenting our in JS use ctrl + /
 // use ctrl+shift+/ for muti-line comment
 // console.log allows the browser to display the message that is inside the parenthesis
 console.log("Hello World")
 console.
 log
 (
    "Hello Again"
)

// Variables - make sure not inside quotation marks un "myVariable"
// it is used to store data pero pwede mong palitan in the future
// any information that is used by an application is stored in what we called "memory"
// when we create variables, certain portions of a device's memory is given "name" that we call "variables"

// if the variable has been called without declaring the variable itself, the console would render that the variable we are calling is "not defined"
// variables are initialized with the use of let/const keyword
// after declaring the variable (na-create na yung variable and it is ready to receive data), failure to assign a value would mean that the variable is "undefined" so nadeclare na pero wala pang na-assign na value

let myVariable = "Hello"

 console.log(myVariable);
/*Guides in writing variables
   -using the right keyword is a way for a dev to successfully initialize a variable (let/const)
   -variable names should start with a lowercase character, and use camelCasing for multiple words
   -variable names should be indicative or descriptive of the value being stored to avoid confusion*/

   let firstName = "Michael";
   let pokemon = 25000;

// other variable examples: lastName emailAdddress mobileNumber
// pokemmon is a bad naming since it does not definitively describe what the variable value is. in this case, number yung value so anong konek niya sa variable name?

// let first name ="Michael";
// console.log(first name)
// pag ganito, magkakaron ng syntax error. kasi may space

// pwede namang underscore pero mas conventional yung camelCasing kaya yun na lang ang gamitin. Pag hyphen, operation siya sa JS (subtraction)


// Decalaring and Initializing Variables
let productName = "Desktop Computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// pag integer or number lang kahit wag ng ipasok sa quotation marks

// In the context of certain applications, some variables/info are constant and should not be changed; pag ganito, ang gagamitin ay const kasi constant na yung value niya
// one example in real-world scenario is the interest for loan, savings account, or mortgage interest must not be changes due to implications in computation
const interest = 3.539;
console.log(interest);

// Reassigning of variable values
// reassigning a variable value means that we are going to change the previous value into another value
/*SYNTAX:
   letVariableName = newValue;
   */
productName = "Laptop";
console.log(productName);

let friend = "Joch";
console.log(friend);

friend = "Yohan";
console.log(friend);

/*interest = 4.489;
console.log(interest);*/
// pag nagdeclare ka ng new value para sa isang constant, mag-error din siya. heto message: Uncaught TypeError: Assignment to constant variable.

/*when to use JS const for a variable?
   as a general rule, always declare a variable with const unless you know that the value will change
*/

// Reassigning vs. initializing
let supplier; 
// this is technically an initialization since we are assigning a value to the variable the first time
supplier="John Smith Tradings";
console.log(supplier);
// the following is reassigning since the value has been reassigned:
supplier="Zuitt Store";
console.log(supplier);

// var vs let/const
a=5;
console.log(a);
var a;

// var is also used in declaring a variable but var is an ECMAScript1 (ES1) feature [Javascript 1997]
// let/const - introduced as new features of ES6 during 2015
/*
anong difference nila? may issues when it comes to declaring variables using var regarding hoisting. in terms of variables, keyword var is hoisted while let/const does not allow hoisting. 
What is hoisting? Hoisting is Javascript's default behavior of moving declarations to the top
*/

// Scope of Variables
// scope essentially means where these variables are available for use (hanggang saan sila kayang ireach ng mga codes)
// let and const variables are block scoped
   // a block is a chunk of codes bounded by {} anything within the curly brace is a block
   // so pag nakalabas na sila sa {} hindi na siya mababasa or mareach. mga kapatid lang niya pwede mag-access sa kanya
   // same goes kahit const ang gagamitin

let outerVariable="hello";
{
   let innerVariable="hello again";
   console.log(innerVariable);
}
console.log(outerVariable);
// console.log(innerVariable);

// Multiple Variable declarations
/*multiple variables can be declared in one line using one keyword. the two variable declarations must be separated by a comma
hanggang ilang variables pwede? wala namang limit but better if hindi gagamitin sa sobrang daming variables para di affected ang code readability*/
   // make sure ang na ang pinagsama mong variables ay parehong const or parehong pwedeng palitan. otherwise, use separate declarations for these cariables
let productCode = "CD017", productBrand = "Dell";
console.log(productCode);
console.log(productBrand);

// trying to use a variable with a reserved keyword
/*const let="hello";
console.log(let);
heto lilitaw: let is disallowed as a lexically bound name */

// Data Types in Javascript
// strings - series of characters that create a word or a phrase/sentence/anything related to creating a text
let country = "Philippines";
console.log(country);

// Concatenating strings
// we are combining multiple variables with string values with the "+" symbol; lilitaw sila sa console na magkakasunod
let province = "Metro Manila";
console.log(province + ", " + country);

// escape character - use back slash \
// \ in strings in combination with other characters can produce different effects
   // \n - would create a new line
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);


// using double quotes and single quotes for string data types are actually valid in JS
console.log("John's employess went home early.");
console.log('John\'s employess went home early.');

// Numbers
// integers/whoe numbers
// with the exception of strings, other data types in JS are color coded
let headcount = 26;
console.log(headcount);

// decimal/fractions
let grade = 98.7;
console.log(grade);

// exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// combining strings and numbers
console.log("John's grade last quarter is " + grade);
// when numbers and strings are combined, the resulting data type would be a string


// Boolean
// are normally used to store values relating to the state of certain things
// true or false for the default value
let isMarried = false;
let inGoodConduct = true;

console.log(isMarried);
console.log(inGoodConduct);


console.log("isMarried: " + isMarried);
console.log("inGoodConduct " + inGoodConduct);


// Array
// special kind of data types that are used to store multiple values
// Arrays can store different data types but it is normally used to store similar data types
/*SYNTAX for Arrays: 
let/const varName = [elementA, elementB, elementC,...elementN]*/

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types
let person = ["John", "Smith", 32, true];
console.log(person);

// it is not advisable to use different data types in an array since it will be confusing for other devs when they read our codes

// object data type 
// objects are another special kid of data type that's used to mimic real world objects
// they are used to create complex data that contains pieces of info that are relevant to each other, kahit pagsamahin mo, they will still make sense
/*SYNTAX:
let/const varName = {
   propertyA: value,
   peropertyB: value
}
*/
// take note of the closing tags
let personDetails = {
   fullName: "Juan Dela Cruz",
   age: 35,
   isMarried: true,
   contact: ["09123456789", "0987654321"],
   address:{
      houseNumber: "345",
      city: "Manila"
   }
};
// wag din kalimutan ang semicolon sa dulo to indicate na siya ang end
console.log(personDetails);

// typeof keyword
// gamitin to pag di ka sure sa data type 
// used if the dev is not sure or wants to assure what the data type of the variable is
console.log(typeof personDetails);

/*Constant Array/Objects
the const keyword is alittle bit misleading when it comes to arrays/objects
it does not define a constant value for arrays/objects. it defines a constant reference to a value. It means we CANNOT reassign a constant value, a constant array, nor a constant object kasi may existing value ka na for the const variable

But you can change the elements of a constant array, the properties of a constant object by putting [0]*/
const anime = ["Naruto", "Slam Dunk", "One Piece"];
console.log(anime);

// would return an error because we have const variable anime = ["Akame ga Kill"];

anime[0] = ["Akame ga Kill"];
console.log(anime);

// pero pwedeng magdagdag ng elements. sa example, pwedeng anime [3] kasi wala naman laman yung position 3 sa array

// Null Data Type
// the following are not null since they have number 0 and string values
let number = 0;
let string = "";
console.log(number);
console.log(string);

// null is used to intentionally express the absence of a value inside a variable in a declaration/initialization
// one clear difference of null vs undefined is that null means that the variable was created and assigned a value that does not hold any value/amount, compared to the undefined which is concerned with creating a variable but was not given any value
let jowa = null;
console.log(jowa);

